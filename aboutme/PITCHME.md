## INTRO TO PYTHON...
## AND USEFUL LIBRARIES

### Data Science Retreat Masterclass

---

## ABOUT ME: Amélie Anglade

- DS and Music IR Consultant
- PhD in MIR from QMUL
- Worked for Large R&D Labs: Sony CSL, Philips Research
- Now works mostly with music startups: SoundCloud
- Use Python for quick prototyping, deployment and scaling
- Co-founder and Python coach at the OpenTechSchool Berlin

---

## HOW TO REACH ME

- #python-masterclass channel on datascienceretreat.slack.com 
- [@utstikkar](https://twitter.com/utstikkar) on Twitter (and the web)
- [amelie.anglade@gmail.com](mailto:amelie.anglade@gmail.com)




