# THE PYTHON PROGRAMMING LANGUAGE

---

# WHY PYTHON ?

---

## WHY "PYTHON"?

  - Monty Python Reference

  - "​When he began implementing Python, Guido van Rossum was also reading the published scripts from “Monty Python's Flying Circus”, a BBC comedy series from the 1970s. Van Rossum thought he needed a name that was short, unique, and slightly mysterious, so he decided to call the language Python." -- General Python FAQ
     
  - So nothing to do with reptiles... except for its logo:

#VSLIDE
@title[LOGO] 
![Logo-Python](https://www.python.org/static/community_logos/python-logo-inkscape.svg)

---

## WHY LEARNING PYTHON?

- In general :
    - Easy to learn
    - Elegant yet simple and compact syntax 
    - Dynamic typing + interpreted language
    - High-level data structures

#VSLIDE
@title[WHY IS IT SO USEFUL?]

- ...
    - Object-Oriented Programming
    - Many libraries 
    - Great for both scripting and software/app development
    - Free and open-source software, with vibrant community
    - Extensible: add new built-in modules to Python (written in C)

#VSLIDE
@title[WHY FOR DS?] 

- For Data Scientists :
  - Very rich scientific computing libraries
  - All DS tasks can be performed with Python: 
    - accessing, collecting, cleaning, analysing, visualising data, modelling, evaluating models, integrating in prod, scaling

---


## PYTHON FOR DS COMPONENTS

#VSLIDE

@title[PYTHON FOR DS COMPONENTS 1] 

- Python​
  - Python language
  - Modules of the standard library
  - A large number of specialised modules and apps written in Python
  - Development tools
- IPython
  - ​an interactive Python shell
  - Jupyter Notebook: web application to create and share documents that contain live code, equations, visualizations and explanatory text

#VSLIDE

@title[PYTHON FOR DS COMPONENTS 2] 
  
- NumPy: p​rovides powerful numerical array objects, and routines to manipulate them
- SciPy: high-level data processing routines. Optimisation, regression, interpolation, etc.
- Matplotlib: 2- and 3-D visualisations and plots​
- Pandas: powerful data structures especially to deal with time series
- Scikit-learn: Machine Learning toolbox
- statsmodels: statistical modelling toolbox (not covered in this course)

---

# PYTHON 2 vs. PYTHON 3

---

### WHAT IS IT ABOUT?

- **2 major versions of Python** in widespread use:
- Python 2.x and Python 3.x
- **Some features in Python 3** are not backward compatible with Python 2
- **Some Python 2** libraries have not been updated to work with Python 3

#VSLIDE

@title[WHAT IS IT ABOUT IN THIS CLASS] 

- I use Python 2 due to such libraries, but as more of them are migrating to Python 3, I'll soon move to it too
- Marek uses mostly Python 3 as all his libraries are ported
- **Bottom-line**: there is no wrong choice, as long as all the libraries you need are supported by the version you choose 
- **In this masterclass**: we'll write Python3 (-compatible) code

---

@title[Print V2 vs. V3]

## Examples

- From **print statements** (Py2) to **print function calls** (Py3):

```python
# Python 2 only:
print 'Hello'

# Python 2 and 3:
print('Hello')

# Python 2 only:
print 'Hello', 'World'

# Python 2 and 3:
from __future__ import print_function
print('Hello', 'World')
```

#VSLIDE

@title[Divisions V2 vs. V3]

- From integer division to true division:

```python
# Python 2 only:
assert 2 / 3 == 0

# Python 2 and 3:
assert 2 // 3 == 0

# Python 3 only:
assert 3 / 2 == 1.5

# Python 2 and 3:
from __future__ import division    # (at top of module)
assert 3 / 2 == 1.5
```
 
---

## MORE on THIS

- To explore and test more differences: [IPython Notebook](https://github.com/rasbt/python_reference/blob/master/tutorials/key_differences_between_python_2_and_3.ipynb) by Sebastian Raschka
     
- To keep your code Python 2 and 3 compatible: a comprehensive [Cheat Sheet](http://python-future.org/compatible_idioms.html#cheat-sheet-writing-python-2-3-compatible-code)

---

## INSTALLING PYTHON
### AND ALL USEFUL PACKAGES

---

@title[Anaconda]

- **[RECOMMENDED]** Anaconda (Windows, Linux, OSX) : 
    - For new Python users who want to install a full Python environment for Data Science
    - Download it [here](http://continuum.io/downloads)
    - Follow the simple installation instructions

#VSLIDE
@title[Manual Installation]

- Manual installation with Python package manager :
    - (Only) if you already have been using Python on your computer for some time 
    - Make sure to install:  Python, pip **and** virtualenv 
    - Follow installation instructions for: [OS X](http://docs.python-guide.org/en/latest/starting/install3/osx/#install3-osx), [Linux](http://docs.python-guide.org/en/latest/starting/install3/linux/) or [Windows](http://docs.python-guide.org/en/latest/starting/install3/win/)
    - Once in your dedicated virtualenv for this masterclass, use pip to install of the necessary packages:
```python
pip install -U numpy, scipy, ipython, matplotlib, pandas, scikit-learn
```	
    **Warning**: this might require some external dependencies to be installed. When in doubt: Google your error messages!

---

## RUNNING
### THE IPYTHON INTERPRETER
### AND A PYTHON FILE

---
## YOUR FIRST PYTHON FILE

- Open your favorite editor
- Create a file hello_world.py
- Inside write the following:
```python
s = 'Hello World!'
print(s)
```
- Now from your shell/terminal/console run:
```python
$ ipython hello_world.py
```
+++
@title[...]
- Then simply run the IPython interpreter:
```python
$ ipython
```  
- And from there run again your hello_world.py file:
```python
%run hello_world.py
```  
- Explore further:
```python
s
i = 2
s2 = 'Hi'
s+s2
```
---

# JUPYTER NOTEBOOK

---

## IN ACTION

### Start IPython Notebook:

```python 
jupyter notebook
```

---

## TUTORIAL

More about Jupyter Notebooks... in this [Jupyter Notebook](http://nbviewer.ipython.org/github/ipython/ipython/blob/2.x/examples/Notebook/Running%20Code.ipynb),
and even more in [this one](http://arogozhnikov.github.io/2016/09/10/jupyter-features.html)


Tip: Try cloning or downloading it and then opening it.
Run and modify its code cells.

---

## PYTHON BASICS

---

## TUTORIAL

Read and test for yourself the examples provided in:

[The SciPy Lectures -- The Python Language](http://scipy-lectures.github.io/intro/language/python_language.html)

Tip: Practice those examples using alternatively python files, the IPython interpreter and an IPython Notebook.

---

## TO PRACTICE

- [Python interactive exercises](http://codingbat.com/python)
     
- [Join the codewars competitions](http://www.codewars.com/?language=python)

